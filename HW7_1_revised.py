import sys
'''
Handbook of Applied Cryptography

a = 640
b = 49
'''
def printf(format, *args):
    sys.stdout.write(format % args)

def eea(a,b):
    
    if a < b:
        return -1
    
    d = 0

    x = 0

    if b == 0:

        d = a

        x = 1

        y = 0

        return d,x,y

    x_2 = 1

    x_1 = 0

    y_2 = 0

    y_1 = 1
    
    i = 1

    while b > 0:

        q = a // b

        r = a - q*b

        x = x_2 - q*x_1

        y = y_2 - q*y_1

        a = b

        b = r

        x_2 = x_1

        x_1 = x

        y_2 = y_1

        y_1 = y

        i += 1
        
        printf("i:%d\tq:%d\tr:%d\tx:%d\ty:%d\ta:%d\tb:%d\n",i,q,r,x,y,a,b)

    d = a

    x = x_2

    y = y_2

    i += 1
    
    printf("i:%d\tq:%d\tr:%d\tx:%d\ty:%d\ta:%d\tb:%d\n",i,q,r,x,y,a,b)

    return d,x,y

print(eea(640,49))

